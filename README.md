# Birthday Greeting Kata

1. You have a set of member records from a db.
2. Get a greeting message to all members whose birthday is today via a RESTful api. (You
can design an api to get greeting msg. You don’t have to send out an email. The result of api is json data.)
---

Notes:
1. Please separate your commit instead of a whole huge one.
2. Each version has its own branch.
3. Please consider it as a real product and write any tests if necessary.
4. Any language is ok.
---

Db records:

|   | First Name | Last Name | Gender | Date of Birth | Email |
|---|------------|-----------|--------|---------------|-------|
|1|Robert|Yen|Male|1985/8/8|robert.yen@linecorp.com|
|2|Cid|Change|Male|1990/10/10|cid.change@linecorp.com|
|3|Miki|Lai|Female|1993/4/5|miki.lai@linecorp.com|
|4|Sherry|Chen|Female|1993/8/8|sherry.lai@linecorp.com|
|5|Peter|Wang|Male|1950/12/22|peter.wang@linecorp.com|

---

### Version 1: Simple Message
> Subject: Happy birthday! 
> 
> Happy birthday, dear Robert!

**Spec by example**

**Given**: a set of member records above

**When**: today is 8/8

**Then**: we will get the message below when a RESTful api is invoked.

> Subject: Happy birthday!
> 
> Happy birthday, dear Robert!


> Subject: Happy birthday!
>
> Happy birthday, dear Sherry!

---

### Version 2: Tailer-made Message for different gender
Male:
> Subject: Happy birthday!
> 
> Happy birthday, dear `Robert`!
> 
> We offer special discount `20%` off for the following items: 
> 
> `White Wine, iPhone X`

Female:
> Subject: Happy birthday!
> 
> Happy birthday, dear `Sherry`!
> 
> We offer special discount `50%` off for the following items: 
> 
> `Cosmetic, LV Handbags`

**Spec by example**

**Given**: a set of member records above,

**When**: today is 8/8,

**Then**: we will get the message below when a RESTful api is invoked.

> Subject: Happy birthday!
> 
> Happy birthday, dear Robert!
> 
> We offer special discount 20% off for the following items: 
> 
> White Wine, iPhone X


> Subject: Happy birthday!
> 
> Happy birthday, dear Sherry!
> 
> We offer special discount 50% off for the following items: 
> 
> Cosmetic, LV Handbags

---

### Version 3: Message with an Elder Picture for those whose age is over 49.

> Subject: Happy birthday! 
> 
> Happy birthday, dear `Peter`! 
> 
> (A greeting picture here)

---

### Version 4: Simple Message with full name

> Subject: Happy birthday!
> 
> Happy birthday, dear `Yen, Robert`!


> Subject: Happy birthday!
> 
> Happy birthday, dear `Chen, Sherry`!

---

### Version 5: Simple Message but database changes

> Subject: Happy birthday!
> 
> Happy birthday, dear Robert!


> Subject: Happy birthday!
> 
> Happy birthday, dear Sherry!

**Please choose another way to persist your member data**

Ex: mongo db ↔ mysql...

---

### Version 6: Simple Message but different output data format


>Subject: Happy birthday!
>
>Happy birthday, dear Robert!

**Please choose another output data format**

JSON:

```
{ 
 "title": "Subject: Happy birthday!", 
 "content": "Happy birthday, dear Robert!"
}
```

XML:

```
<root>
 <title>Subject: Happy birthday!</title> 
 <content>Happy birthday, dear Robert!</content>
</root>
```
