DROP TABLE user_profile;
CREATE TABLE user_profile
(
    user_id    INTEGER      NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(64)  NOT NULL,
    last_name  VARCHAR(64)  NOT NULL,
    gender     VARCHAR(8)   NOT NULL,
    birthday   DATE         NOT NULL,
    email      VARCHAR(128) NOT NULL,
    PRIMARY KEY (user_id)
);