INSERT INTO user_profile (first_name, last_name, gender, birthday, email)
VALUES ('Robert', 'Yen', 'Male', '1985-08-08', 'robert.yen@linecorp.com');
INSERT INTO user_profile (first_name, last_name, gender, birthday, email)
VALUES ('Cid', 'Change', 'Male', '1990-10-10', 'cid.change@linecorp.com');
INSERT INTO user_profile (first_name, last_name, gender, birthday, email)
VALUES ('Miki', 'Lai', 'Female', '1993-04-05', 'miki.lai@linecorp.com');
INSERT INTO user_profile (first_name, last_name, gender, birthday, email)
VALUES ('Sherry', 'Chen', 'Female', '1993-08-08', 'sherry.lai@linecorp.com');
INSERT INTO user_profile (first_name, last_name, gender, birthday, email)
VALUES ('Peter', 'Wang', 'Male', '1950-12-22', 'peter.wang@linecorp.com');