package kata.birthday.greeting.validation;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class GreetingValidation {

    /**
     * @param birthday mm-dd, ex: 08-08, 8-8
     */
    public static void validateBirthday(String birthday){
        String leapYearDateString = "2000-" + birthday;
        try {
            LocalDate.parse(leapYearDateString);
        }catch (DateTimeParseException e){
            throw new IllegalArgumentException("Invalid mm-dd of " + birthday);
        }
    }
}
