package kata.birthday.greeting.controller;

import kata.birthday.greeting.api.GreetingApi;
import kata.birthday.greeting.model.Greeting;
import kata.birthday.greeting.service.GreetingService;
import kata.birthday.greeting.validation.GreetingValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class GreetingController implements GreetingApi {

    @Resource
    GreetingService greetingService;

    @Override
    public ResponseEntity<List<Greeting>> greetingBirthdayGet(String birthday) {
        GreetingValidation.validateBirthday(birthday);
        int month = Integer.parseInt(birthday.split("-")[0]);
        int day = Integer.parseInt(birthday.split("-")[1]);
        List<Greeting> greetingList = greetingService.getGreetingByBirthDay(month, day);
        return new ResponseEntity<>(greetingList, HttpStatus.OK);
    }
}
