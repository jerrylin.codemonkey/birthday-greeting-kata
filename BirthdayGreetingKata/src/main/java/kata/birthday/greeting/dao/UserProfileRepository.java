package kata.birthday.greeting.dao;

import kata.birthday.greeting.entity.UserProfile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProfileRepository extends CrudRepository<UserProfile, Long>, UserProfileDao {
    @Override
    @Query(value = "SELECT * FROM user_profile WHERE MONTH(birthday) = :month AND DAY_OF_MONTH(birthday) = :day", nativeQuery = true)
    List<UserProfile> findByBirthday(int month, int day);
}
