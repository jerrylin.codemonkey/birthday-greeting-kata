package kata.birthday.greeting.dao;

import kata.birthday.greeting.entity.UserProfile;

import java.util.List;

public interface UserProfileDao {
    List<UserProfile> findByBirthday(int month, int day);
}
