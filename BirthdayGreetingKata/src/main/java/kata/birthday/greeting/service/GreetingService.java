package kata.birthday.greeting.service;

import kata.birthday.greeting.model.Greeting;

import java.util.List;

public interface GreetingService {
    List<Greeting> getGreetingByBirthDay(int month, int day);
}
