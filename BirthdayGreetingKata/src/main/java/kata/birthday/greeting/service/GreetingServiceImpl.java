package kata.birthday.greeting.service;

import kata.birthday.greeting.dao.UserProfileRepository;
import kata.birthday.greeting.entity.UserProfile;
import kata.birthday.greeting.model.Greeting;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GreetingServiceImpl implements GreetingService {
    @Resource
    UserProfileRepository userProfileDao;

    @Override
    public List<Greeting> getGreetingByBirthDay(int month, int day) {
        List<UserProfile> userProfileList = userProfileDao.findByBirthday(month, day);
        return userProfileList.stream().map(this::toGreeting).collect(Collectors.toList());
    }

    private Greeting toGreeting(UserProfile source) {
        Greeting target = new Greeting();
        target.setTitle("Subject: Happy birthday!");
        target.setContent(String.format("Happy birthday, dear %s!", source.getFirstName()));
        return target;
    }
}
