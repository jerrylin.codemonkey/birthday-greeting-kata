package kata.birthday.greeting.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GreetingValidationTest {

    @Test
    void validateBirthday() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> GreetingValidation.validateBirthday("01"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> GreetingValidation.validateBirthday("-01"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> GreetingValidation.validateBirthday("-1-01"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> GreetingValidation.validateBirthday("13-01"));

        GreetingValidation.validateBirthday("02-29");
    }
}