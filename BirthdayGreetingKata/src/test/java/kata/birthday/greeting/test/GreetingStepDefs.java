package kata.birthday.greeting.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import kata.birthday.greeting.model.Greeting;

import java.util.List;

import static io.cucumber.core.options.Constants.PLUGIN_PROPERTY_NAME;
import static io.restassured.RestAssured.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GreetingStepDefs extends SpringIntegrationTest{
    static String response = null;

    ObjectMapper objectMapper = new ObjectMapper();

    @Given("A set of member records")
    public void aSetOfMemberRecords() {
    }
    @When("When the client calls GET with {string}")
    public void whenTheClientCallsGETWith(String birthday) {
        System.out.println("birthday: " + birthday);
        response = when().
                get("http://localhost:8080/greeting/{birthday}", birthday).
                then().
                statusCode(200).extract().asString();
    }
    @Then("The client receives {string} items")
    public void theClientReceivesItems(String expectedNumberOfItems) throws JsonProcessingException {
        List<Greeting> greetingList = objectMapper.readValue(response, new TypeReference<List<Greeting>>(){});
        assertEquals(Integer.valueOf(expectedNumberOfItems), greetingList.size());
    }
}
