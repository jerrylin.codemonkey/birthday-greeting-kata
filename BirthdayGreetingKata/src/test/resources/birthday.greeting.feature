Feature: Get a greeting message
  Get a greeting message to all members whose birthday is today via a RESTful api.

  Scenario Outline: client makes call to GET /greeting/{birthday}
    Given A set of member records
    When When the client calls GET with "<birthday>"
    Then The client receives "<number>" items

    Examples:
      | birthday | number |
      | 08-08    | 2      |
      | 10-10    | 1      |