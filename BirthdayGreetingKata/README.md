### Note

The main branch is for a runnable Version 1.

The remaining versions as describe in the Kata will be in the feature branches base on the main branch.

### Project Structure

```

├── api           # Interface for rest api
├── controller    # Implementations of the rest api
├── doa           # Data Access layer
├── entity        # Entity models
├── handler       # Exception handlers
├── model         # View models
├── service       # Service layer
├── validation    # Validations
```

### Building block

```
├── Controller
│   ├── Service
│   │   ├── DAO
```

### CMD

```
mvn spring-boot:run
```